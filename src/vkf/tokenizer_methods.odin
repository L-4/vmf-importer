package vkf;

char_upper :: proc (char: u8) -> u8 {
    // Do we really have no 'not' operation? Yes we do, fix.
    return char & (((1 << 8) - 1) ~ (1 << 5));
}

char_lower :: proc (char: u8) -> u8 {
    return char | (1 << 5);
}

is_alphabetic :: proc (char: u8) -> bool {
    upper := char_upper(char);
    return upper >= 'A' && upper <= 'Z';
}

is_numeric :: proc (char: u8) -> bool {
    return char >= '0' && char <= '9';
}

is_quote :: proc (char: u8) -> bool {
    return char == '"' || char == '\'';
}

is_identifier_start :: proc (char: u8) -> bool {
    return is_alphabetic(char) ||
           char == '_' ||
           char == '$';
}

// @todo: Unify with is_identifier_start.
is_identifier_body :: proc (char: u8) -> bool {
    return is_alphabetic(char) ||
           is_numeric(char) ||
           char == '_';
}

is_numeric_start :: proc (char: u8) -> bool {
    return is_numeric(char) ||
           char == '-' ||
           char == '.' ||
           char == '+';
}

is_numeric_body :: proc (char: u8) -> bool {
    return is_numeric(char) ||
           char == '.';
}
