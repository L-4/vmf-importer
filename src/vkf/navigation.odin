package vkf

import "core:fmt"

Token_Stream :: struct {
    using file: File,
    pos: int,
}

// @todo: Would be less garbage to do this all immutably
// @todo: We could sort tokens in place earlier in the process, then we can
// jump to classes/properties quicker.

get_token_string :: proc (using stream: ^Token_Stream, ofs: int) -> string {
    return string(tokens[ofs].contents);
}

get_token_slice :: proc (using stream: ^Token_Stream, ofs: int) -> []byte {
    return tokens[ofs].contents;
}

// Returns true if move succeeded
// This is probably the only time that you'd not care about scope, since you might
// choose to do so as to jump to a section of the file.
move_to_class :: proc (class_name: string, using stream: ^Token_Stream, in_scope := false) -> bool {
    depth := 0;
    for ; pos < len(tokens); pos += 1 {
        // fmt.println(pos, class_name);
        token := tokens[pos];
        if token.type == .Class {
            if string(token.contents) == class_name {
                // @todo: Do we need a special proc to do this? This is magic, but it should (?) work.
                pos += 2;
                return true;
            }
        } else if in_scope && token.type == .OpenBracket {
            depth += 1;
        } else if in_scope && token.type == .CloseBracket {
            depth -= 1;

            if depth < 0 do return false;
        }
    }

    return false;
}

exit_scope :: proc (using stream: ^Token_Stream) {
    depth := 0;

    for ;pos < len(tokens); pos += 1 {
        #partial switch tokens[pos].type {
            case .CloseBracket:
                depth -= 1;
                if depth < 0 {
                    pos += 1;
                    return;
                }
            case .OpenBracket:
                depth += 1;
        }
    }

}

// This sucks in a major way.
// Get offset to next property by name.
get_property_ofs :: proc (using stream: ^Token_Stream, property: string) -> int {
    ofs := pos;
    depth := 0;
    for ; ofs < len(tokens); ofs += 1 {
        token := tokens[ofs];

        #partial switch token.type {
            case .Key:
                if get_token_string(stream, ofs) == property do return ofs + 1;
            case .OpenBracket:
                depth += 1;
            case .CloseBracket:
                depth -= 1;
                if depth < 0 do return -1;
        }
    }

    return -1;
}

has_class_in_scope :: proc (using stream: ^Token_Stream, class_name: string) -> bool {
    for depth, ofs := 0, pos; ofs < len(tokens); ofs += 1 {
        token := tokens[ofs];
        #partial switch token.type {
            case .Class:
                // Only find at depth 0.
                // @todo: This sucks. Why do we have to compare strings?
                if depth == 0 && get_token_string(stream, ofs) == class_name {
                    return true;
                }
            case .OpenBracket:
                depth += 1;
            case .CloseBracket:
                depth -= 1;
                if depth < 0 do return false;
        }
    }

    return false;
}
