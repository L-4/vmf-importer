package main

import "core:fmt"
import "core:os"
import "core:strings"

import "vkf"
import "vmf"
import "mathx"
import "timing"

// Takes path, returns
// Directory name, not including trailing slash
// Filename, not including extension
// Extension
get_filename_components :: proc (path: string)
    -> (dirname: string, filename: string, extension: string) {

    i := len(path) - 1;

    // Eat until period.
    for ; i > 0 && path[i] != '.'; i -= 1 {}
    extension = string(path[i:len(path)]);
    extension_start := i;

    // @todo: This super sucks. File must contain all three components,
    // or it dies.

    // Eat until slash
    for ; i > 0 && path[i] != '/'; i -= 1 {}
    filename = string(path[i+1:extension_start]);
    dirname = string(path[0:i]);

    return dirname, filename, extension;
}

main :: proc () {
    timing.run_start();
    if len(os.args) <= 1 {
        fmt.println("Usage vmf <filename>");
        os.exit(1);
    }

    filename := string(os.args[1]);
    fmt.printf("Converting %s...\n", filename);

    dirname, basename, _ext := get_filename_components(filename);

    obj_file_output := [4]string { dirname, "/", basename, ".obj" };

    obj_file_name := strings.concatenate(obj_file_output[:]);

    timing.parse_start();

    vkf_file := vkf.parse_file(filename);
    defer vkf.free_vkf_file_data(&vkf_file);

    vmf.build_vmf_model(vkf_file, obj_file_name);
}
