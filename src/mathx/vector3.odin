package mathx

import "core:math"

V3 :: [3]f32;

v3_cross :: proc (v1: V3, v2: V3) -> V3 {
    return V3 {
        v1.y * v2.z - v1.z * v2.y,
        v1.z * v2.x - v1.x * v2.z,
        v1.x * v2.y - v1.y * v2.x,
    };
}

v3_dot :: proc (v1: V3, v2: V3) -> f32 {
    return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

v3_magnitude2 :: proc (vec: V3) -> f32 {
    return vec.x*vec.x+vec.y*vec.y+vec.z*vec.z;
}

v3_normalize :: proc(vec: V3) -> V3 {
    return vec / math.sqrt(v3_magnitude2(vec));
}
