package mathx

// @todo: This is dumb.
Plane :: struct {
    normal: V3,
    offset: f32,
}

plane_cross :: proc (p1: Plane, p2: Plane) -> V3 {
    return v3_cross(p1.normal, p2.normal);
}

plane_dot :: proc (p1: Plane, p2: V3) -> f32 {
    return v3_dot(p1.normal, p2) - p1.offset;
}

intersect_planes :: proc (p1: Plane, p2: Plane, p3: Plane) -> (bool, V3)
{
    determinant := v3_dot(v3_cross(p1.normal, p2.normal), p3.normal);

    if determinant*determinant < 0.001 {
        return false, V3 {};
    } else {
        return true, (p1.offset * v3_cross(p2.normal, p3.normal) +
                     p2.offset * v3_cross(p3.normal, p1.normal) +
                     p3.offset * v3_cross(p1.normal, p2.normal)) / determinant;
    }
}

plane_from_points :: proc (p1: V3, p2: V3, p3: V3) -> Plane {
    // @todo Deal with duplicate v1/2/3 or everything dies

    p1p2 := p2 - p1;
    p1p3 := p3 - p1;

    plane: Plane = ---;

    plane.normal = v3_normalize(v3_cross(p1p2, p1p3));
    plane.offset = v3_dot(p1, plane.normal);

    return plane;
}
