package timing

import "core:time"
import "core:fmt"

Time :: time.Time;

Timing :: struct {
    run_start: Time,
    parse_start: Time,
    build_ir_start: Time,
    build_triangles_start: Time,
    output_obj_start: Time,

    end: Time,
}

global_timing := Timing {};

run_start :: proc () { global_timing.run_start = time.now(); }
parse_start :: proc () { global_timing.parse_start = time.now(); }
build_ir_start :: proc () { global_timing.build_ir_start = time.now(); }
build_triangles_start :: proc () { global_timing.build_triangles_start = time.now(); }
output_obj_start :: proc () { global_timing.output_obj_start = time.now(); }

end :: proc () { global_timing.end = time.now(); print(); }

print :: proc () {
    using global_timing;

    setup_time := cast(f32)time.diff(run_start, parse_start) / (1000.0 * 1000.0);
    parse_time := cast(f32)time.diff(parse_start, build_ir_start) / (1000.0 * 1000.0);
    build_ir_time := cast(f32)time.diff(build_ir_start, build_triangles_start) / (1000.0 * 1000.0);
    build_triangle_time := cast(f32)time.diff(build_triangles_start, output_obj_start) / (1000.0 * 1000.0);
    output_obj_time := cast(f32)time.diff(output_obj_start, end) / (1000.0 * 1000.0);

    total_time := cast(f32)time.diff(run_start, end) / (1000.0 * 1000.0);

    fmt.printf("setup: %.2fms | parse: %.2fms | ir: %.2fms | tris: %.2fms | IO: %.2fms | total: %.2fms\n",
        setup_time,
        parse_time,
        build_ir_time,
        build_triangle_time,
        output_obj_time,
        total_time);
}
