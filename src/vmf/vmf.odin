package vmf

import "core:fmt"
import "core:strconv"
import "core:os"
import "core:slice"

import "../vkf"
import "../mathx"
import "../timing"

Plane :: mathx.Plane;
V3 :: mathx.V3;

Triangle :: struct {
    p1, p2, p3: V3,
}

build_vmf_model :: proc (vkf_file: vkf.File, output_file: string) {
    timing.build_ir_start();
    ir := parse_vmf_ir(vkf_file);
    triangles := [dynamic]Triangle{};

    timing.build_triangles_start();
    for solid in ir.world.solids {
        if solid.anySideHasDisplacement do continue;
        sides := solid.sides;
        // fmt.println(sides);
        for f1 in 0..<len(sides) { // f1 = target face.
            // Lets' ignore tools for now.
            if sides[f1].material[:6] == "tools/" do continue;
            vertices := [dynamic]V3 {};
            // intersections: u32 = 0;
            // f2 & f3 just need to be permutations of sides - f1
            for f2 in 0..<len(sides) {
                if f2 == f1 do continue;
                for f3 in f2+1..<len(sides) {
                    if f3 == f1 do continue;
                    intersected, intersection := mathx.intersect_planes(
                        sides[f1].plane,
                        sides[f2].plane,
                        sides[f3].plane
                    );

                    if intersected {
                        // intersections |= (1 << cast(u32)f2);

                        should_cull := false;
                        for f4 in 0..<len(sides) {
                            if f4 == f1 || f4 == f2 || f4 == f3 do continue;

                            if mathx.plane_dot(sides[f4].plane, intersection) < 0.0 {
                                // fmt.println("Intersection was over", f4);
                                should_cull = true;
                                break;
                            }
                        }

                        if !should_cull do append(&vertices, intersection);
                    }
                }
            }

            if ODIN_DEBUG && len(vertices) < 3 {
                fmt.println("Failed generating mesh for solid: only got", len(vertices), "vertices");

                fmt.println("Failed on:");
                for gf1 in 0..<len(sides) {
                    fmt.printf("%d ", gf1);
                    if gf1 == f1 { fmt.printf(" -> "); } else { fmt.printf("    "); }
                    plane := sides[gf1].plane;
                    fmt.println("{", plane.normal.x, ",",
                                     plane.normal.y, ",",
                                     plane.normal.z, ",",
                                     plane.offset, "}");
                }
            }

            // fmt.println("vertex count", len(vertices));
            // assert(len(vertices) >= 3);

            if len(vertices) >= 3 {
                Plane_Data :: struct {
                    normal, middle: V3,
                };

                plane_data := Plane_Data {
                    normal = sides[f1].plane.normal,
                };

                for vertex in vertices do plane_data.middle += vertex;
                plane_data.middle /= cast(f32)len(vertices);


                context.user_ptr = &plane_data;
                slice.sort_by(vertices[:], proc (i, j: V3) -> bool {
                    using plane_data := (^Plane_Data)(context.user_ptr);

                    return mathx.v3_dot(normal, mathx.v3_cross((i - middle), (j - middle))) <= 0.0;
                });

                for v_idx in 1..<len(vertices)-1 {
                    append(&triangles, Triangle {
                        p1 = vertices[0],
                        p2 = vertices[v_idx],
                        p3 = vertices[v_idx+1]
                    });
                }
            }
        }
    }

    timing.output_obj_start();

    of, err := os.open(output_file, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, os.S_IRUSR|os.S_IWUSR);
    defer os.close(of);
    // /shrug
    if err != os.ERROR_NONE do fmt.panicf("Oopsie");

    write_str :: proc (of: os.Handle, val: string) {
        as_u8 := transmute([]u8)val;
        written, write_error := os.write(of, as_u8);
        if write_error != os.ERROR_NONE do fmt.panicf("Oops: %d", cast(int)write_error);
    }

    write_ln :: proc (of: os.Handle) {
        as_u8 := [1]u8 { '\n' };
        written, write_error := os.write(of, as_u8[:]);
        if write_error != os.ERROR_NONE do fmt.panicf("Oops: %d", cast(int)write_error);
    }

    for triangle in triangles {
        write_str(of, fmt.tprintf("v %f %f %f\n", triangle.p1[0], triangle.p1[1], triangle.p1[2]));
        write_str(of, fmt.tprintf("v %f %f %f\n", triangle.p2[0], triangle.p2[1], triangle.p2[2]));
        write_str(of, fmt.tprintf("v %f %f %f\n", triangle.p3[0], triangle.p3[1], triangle.p3[2]));
    }

    write_ln(of);

    for idx := 1; idx < len(triangles) * 3 + 1; idx += 3 {
        write_str(of, fmt.tprintf("f %d %d %d\n", idx, idx + 1, idx + 2));
    }

    timing.end();
}
